<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;

use Drupal\spreadsheet_importer\MappingItem;
use Drupal\spreadsheet_importer\Plugin\ProcessorBase;

/**
 * Process nodes.
 *
 * @Processor(
 *   id = "node",
 *   label = @Translation("Node"),
 *   description = @Translation("Process node content.")
 * )
 */
class NodeProcessor extends ProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function insertItem(array $data, array $mapping) {
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $node_data = [
      'type' => $this->configuration['node_type'],
    ];
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->create($node_data);

    foreach ($mapping as $mapping_item) {
      $value = $data[$mapping_item['source']];
      if (isset($value) && !empty($value)) {
        $field_processor = $field_plugin_manager->createInstance($mapping_item['field'], $mapping_item);

        $field_processor_definition = $field_processor->getPluginDefinition();
        if ($mapping_item['target'] == 'title') {
          $node->setTitle($value);
        }
        else {
          $field_value = $field_processor->processField($node, $value);
          if (!$field_processor_definition['external']) {
            $node->{$mapping_item['target']}->setValue($field_value);
          }
        }
      }

    }

    $node->save();

    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates() {
    $target_cadidates = array();
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $this->configuration['node_type']);
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');

    foreach ($fields as $key => $field) {
      $field_plugins = $field_plugin_manager->getFieldPlugin($field->getType());
      if (is_array($field_plugins)) {
        foreach ($field_plugins as $field_plugin) {
          $field_candidates = $field_plugin->getTargetCandidates($field);
          $target_cadidates = array_merge($target_cadidates, $field_candidates);
        }
      }
    }

    return $target_cadidates;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state) {
    $form = array();

    // Load node content types list.
    $query = \Drupal::entityQuery('node_type');
    $results = $query->sort('name')
      ->execute();
    $node_type_options = array();
    foreach ($results as $node_type_id) {
      $node_type = NodeType::load($node_type_id);
      $node_type_options[$node_type_id] = $node_type->get('name');
    }

    $form['node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Node type'),
      '#options' => $node_type_options,
      '#default_value' => isset($this->configuration['node_type']) ? $this->configuration['node_type'] : NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldProcessorForTarget($target) {
    $field_name = explode(":", $target)[0];
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $this->configuration['node_type']);
    return $field_plugin_manager->getFieldPlugin($fields[$field_name]->getType());
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldType($target) {
    $field_name = explode(":", explode(";", $target)[0])[0];
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $this->configuration['node_type']);
    return $fields[$field_name]->getType();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(MappingItem $mapping) {
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $this->configuration['node_type']);
    $target = explode(':', $mapping->getTarget());
    return $fields[$target[0]];
  }

  /**
   * {@inheritdoc}
   */
  public function updateItem($entity_id, array $data, array $mapping) {
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($entity_id);

    foreach ($mapping as $mapping_item) {
      if (isset($data[$mapping_item['source']])) {
        $field_processor = $field_plugin_manager->createInstance($mapping_item['field'], $mapping_item);
        $field_processor_definition = $field_processor->getPluginDefinition();
        $value = $data[$mapping_item['source']];
        if ($mapping_item['target'] == 'title') {
          $node->setTitle($value);
        }
        else {
          $value = $field_processor->processField($node, $value, $mapping);
          if (!$field_processor_definition['external']) {
            $node->{$mapping_item['target']}->setValue($value);
          }
        }
      }
    }

    $node->save();

    return $node;
  }

  /**
   *
   */
  public function exportItem($node_id, array $mappings) {
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $node = Node::load($node_id);
    $row = array();
    $i = 0;
    foreach ($mappings as $mapping) {
      $field_processor = $field_plugin_manager->createInstance($mapping->getField(), $mapping->getConfiguration());
      $row[$i] = $field_processor->exportField($node);
      $i++;
    }
    return $row;
  }

}
