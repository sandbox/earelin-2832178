<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;

use Drupal\spreadsheet_importer\MappingItem;
use Drupal\spreadsheet_importer\Plugin\ProcessorBase;

/**
 * Process taxonomy terms.
 *
 * @Processor(
 *   id = "taxonomy",
 *   label = @Translation("Taxonomy"),
 *   description = @Translation("Process taxonomy terms content.")
 * )
 */
class TaxonomyProcessor extends ProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function insertItem(array $data, array $mapping) {

  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates() {
    $target_cadidates = array();
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('taxonomy_term', $this->configuration['vocabulary']);
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');

    foreach ($fields as $key => $field) {
      $field_plugin = $field_plugin_manager->getFieldPlugin($field->getType());
      if ($field_plugin) {
        $field_candidates = $field_plugin->getTargetCandidates($field);
        $target_cadidates = array_merge($target_cadidates, $field_candidates);
      }
    }

    return $target_cadidates;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state) {
    $form = array();

    // Load vocabularies list.
    $query = \Drupal::entityQuery('taxonomy_vocabulary');
    $results = $query->sort('name')
      ->execute();
    $vocabulary_options = array();
    foreach ($results as $vocabulary_vid) {
      $vocabulary = Vocabulary::load($vocabulary_vid);
      $vocabulary_options[$vocabulary_vid] = $vocabulary->get('name');
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $vocabulary_options,
      '#default_value' => isset($this->configuration['vocabulary']) ? $this->configuration['vocabulary'] : NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldProcessorForTarget($target) {
    $field_name = explode(":", $target)[0];
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('taxonomy_term', $this->configuration['vocabulary']);
    return $field_plugin_manager->getFieldPlugin($fields[$field_name]->getType());
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldType($target) {
    $field_name = explode(":", $target)[0];
    $field_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.field');
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('taxonomy_term', $this->configuration['vocabulary']);
    return $fields[$field_name]->getType();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(MappingItem $mapping) {

  }

}
