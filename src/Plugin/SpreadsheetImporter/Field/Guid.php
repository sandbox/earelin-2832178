<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormStateInterface;
use Drupal\spreadsheet_importer\Plugin\FieldBase;

/**
 * GUID field plugin.
 *
 * @Field(
 *   id = "guid",
 *   label = @Translation("GUID"),
 *   fieldTypes = {"*"},
 *   external = TRUE
 * ),
 */
class Guid extends FieldBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $field_definition) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    return [
      'guid;guid' => $this->t('GUID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processField($node, $field_value) {
    // No action with void field.
  }

  /**
   *
   */
  public function exportField($entity) {
    $imported_item = Database::getConnection()
      ->select('spreadsheet_importer', 'si')
      ->fields('si')
      ->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id())
      ->execute()
      ->fetch();

    if ($imported_item) {
      return $imported_item->guid;
    }
    return '';
  }

}
