<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

use Drupal\spreadsheet_importer\Plugin\FieldBase;

/**
 * Taxonomy reference field plugin.
 *
 * @Field(
 *   id = "taxonomy_reference",
 *   label = @Translation("Taxonomy reference"),
 *   fieldTypes = {
 *     "entity_reference",
 *   },
 *   external = FALSE
 * )
 */
class TaxonomyReference extends FieldBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $field_definition) {
    $form = array();

    if ($this->configuration['subtarget'] == 'name') {
      $form['auto_create'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Auto create terms.'),
        '#default_value' => isset($this->configuration['field_configuration']['auto_create'])
        ? $this->configuration['field_configuration']['auto_create'] : NULL,
      ];
    }

    // Stores field's vocabulary bundle.
    $handler_settings = $field_definition->getSettings()['handler_settings'];
    $form['vocabulary'] = [
      '#type' => 'value',
      '#value' => array_keys($handler_settings['target_bundles'])[0],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    if ($field->getType() == 'entity_reference'
          && $field->getSettings()['target_type'] == 'taxonomy_term') {
      return [
        $field->getName() . ':id;taxonomy_reference;' . $field->getType() => $field->getLabel() . ' (id)',
        $field->getName() . ':name;taxonomy_reference;' . $field->getType() => $field->getLabel() . ' (name)',
      ];
    }
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function processField($node, $field_value) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];
    $vocabulary = $this->configuration['field_configuration']['vocabulary'];
    $auto_create = $this->configuration['field_configuration']['auto_create'];

    if (!is_array($field_value)) {
      $field_value = array($field_value);
    }

    $values = array();
    foreach ($field_value as $field_value_item) {
      if (!empty($field_value_item)) {
        if ($subtarget == 'id') {
          $values[]['target_id'] = $field_value_item;
        }
        else {
          $terms = taxonomy_term_load_multiple_by_name($field_value_item, $vocabulary);
          if (empty($terms)) {
            if ($auto_create) {
              $terms[0] = Term::create([
                'vid' => $vocabulary,
                'name' => $field_value_item,
              ]);
              $terms[0]->save();
            }
            else {
              continue;
            }
          }
          $values[]['target_id'] = array_shift($terms)->id();
        }
      }
    }

    return $values;
  }

  /**
   *
   */
  public function exportField($entity) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];
    $output = array();
    foreach ($entity->$target as $value) {
      $term = Term::load($value->getValue()['target_id']);
      if ($subtarget == 'id') {
        $output[] = $term->id();
      }
      else {
        $output[] = $term->label();
      }
    }

    if (count($output) == 1) {
      $output = $output[0];
    }
    return $output;
  }

}
