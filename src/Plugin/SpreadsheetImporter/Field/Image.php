<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

/**
 * Image field plugin.
 *
 * @Field(
 *   id = "image",
 *   label = @Translation("Image"),
 *   fieldTypes = {
 *     "image",
 *   },
 *   external = FALSE
 * )
 */
class Image extends File {

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    if ($field->getType() == 'image') {
      return [
        $field->getName() . ':uri;image;' . $field->getType() => $field->getLabel() . ' (uri)',
        $field->getName() . ':alt;image;' . $field->getType() => $field->getLabel() . ' (alt)',
        $field->getName() . ':title;image;' . $field->getType() => $field->getLabel() . ' (title)',
      ];
    }
    return NULL;
  }

}
