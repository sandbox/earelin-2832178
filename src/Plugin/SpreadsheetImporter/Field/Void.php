<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\spreadsheet_importer\Plugin\FieldBase;

/**
 * Void field plugin.
 *
 * @Field(
 *   id = "void",
 *   label = @Translation("Void"),
 *   fieldTypes = {"*"},
 *   external = TRUE
 * ),
 */
class Void extends FieldBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $field_definition) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    return [
      'void;void' => $this->t('Void'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processField($node, $field_value) {
    // No action with void field.
  }

  /**
   *
   */
  public function exportField($entity) {
    return '';
  }

}
