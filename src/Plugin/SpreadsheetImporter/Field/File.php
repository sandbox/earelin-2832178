<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File as DrupalFile;

use Drupal\spreadsheet_importer\Plugin\FieldBase;

/**
 * File field plugin.
 *
 * @Field(
 *   id = "file",
 *   label = @Translation("File"),
 *   fieldTypes = {
 *     "file",
 *   },
 *   external = FALSE
 * )
 */
class File extends FieldBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $field_definition) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    if ($field->getType() == 'file') {
      return [
        $field->getName() . ':uri;file;' . $field->getType() => $field->getLabel() . ' (uri)',
        $field->getName() . ':description;file;' . $field->getType() => $field->getLabel() . ' (description)',
      ];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function processField($entity, $field_value) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];
    if (!is_array($field_value)) {
      $field_value = array($field_value);
    }
    $previous_values = $entity->{$target}->getValue();

    $i = 0;
    if ($subtarget == 'uri') {
      $file_folder = 'public://importers';
      foreach ($field_value as $field_value_item) {
        $data = file_get_contents($field_value_item);
        $file = file_save_data($data, $file_folder . basename($field_value_item));
        $previous_values[$i]['target_id'] = $file->id();
        $i++;
      }
    }
    else {
      foreach ($field_value as $field_value_item) {
        $previous_values[$i][$subtarget] = $field_value_item;
        $i++;
      }
    }

    return array_slice($previous_values, 0, $i);
  }

  /**
   *
   */
  public function exportField($entity) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];
    $output = array();
    foreach ($entity->$target as $file_field) {
      $value = $file_field->getValue();
      if ($subtarget == 'uri') {
        $file = DrupalFile::load($value['target_id']);
        $output[] = $file->url();
      }
      else {
        $output[] = $value[$subtarget];
      }
    }

    if (count($output) == 1) {
      $output = $output[0];
    }
    return $output;
  }

}
