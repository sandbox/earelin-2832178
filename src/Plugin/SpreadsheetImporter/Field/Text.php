<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\spreadsheet_importer\Plugin\FieldBase;

/**
 * Text field plugin.
 *
 * @Field(
 *   id = "text",
 *   label = @Translation("Text"),
 *   fieldTypes = {
 *     "string",
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   external = FALSE
 * )
 */
class Text extends FieldBase {

  /**
   * Return the form for long text.
   */
  private function getTextLongForm() {
    $form = array();

    $filters_query = \Drupal::entityQuery('filter_format');
    $filter_ids = $filters_query->sort('name')
      ->execute();
    $filters_storage = \Drupal::entityTypeManager()->getStorage('filter_format');
    $filters = $filters_storage->loadMultiple($filter_ids);
    $filter_options = array();
    foreach ($filters as $filter) {
      $filter_options[$filter->id()] = $filter->label();
    }

    $form['text_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format'),
      '#options' => $filter_options,
      '#default_value' => isset($this->configuration['field_configuration']['text_format'])
      ? $this->configuration['field_configuration']['text_format'] : NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $field_definition) {
    $form = array();

    // Text format for long text fields.
    if ($this->configuration['field_type'] == 'text_long') {
      $form = $this->getTextLongForm();
    }
    elseif ($this->configuration['field_type'] == 'text_with_summary') {
      if ($this->configuration['subtarget'] == 'value') {
        $form = $this->getTextLongForm();
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates($field) {
    switch ($field->getType()) {
      case 'string':
      case 'text_long':
        return [$field->getName() . ':value;text;' . $field->getType() => $field->getLabel()];

      case 'text_with_summary':
        return [
          $field->getName() . ':summary;text;' . $field->getType() => $field->getLabel() . ' (summary)',
          $field->getName() . ':value;text;' . $field->getType() => $field->getLabel() . ' (value)',
        ];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function processField($entity, $field_value) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];

    if (!is_array($field_value)) {
      $field_value = array($field_value);
    }

    $values = array();
    switch ($this->configuration['field_type']) {
      case 'string':
        foreach ($field_value as $field_value_item) {
          $values[] = ['value' => $field_value_item];
        }
        return $values;

      case 'text_long':
        foreach ($field_value as $field_value_item) {
          $values[] = [
            'value' => $field_value_item,
            'format' => $this->configuration['field_configuration']['text_format'],
          ];
        }
        return $values;

      case 'text_with_summary':
        $previous_values = $entity->{$target}->getValue();
        $i = 0;
        foreach ($field_value as $field_value_item) {
          $previous_values[$i][$subtarget] = $field_value_item;
          if ($subtarget == 'value') {
            $previous_values[$i]['format'] = $this->configuration['field_configuration']['text_format'];
          }
          $i++;
        }
        return array_slice($previous_values, 0, $i);

    }

    return FALSE;
  }

  /**
   *
   */
  public function exportField($entity) {
    $target = $this->configuration['target'];
    $subtarget = $this->configuration['subtarget'];
    $values = $entity->$target->getValue();
    $output = array();

    foreach ($values as $value) {
      $output[] = $value[$subtarget];
    }

    if (count($output) == 1) {
      $output = $output[0];
    }
    return $output;
  }

}
