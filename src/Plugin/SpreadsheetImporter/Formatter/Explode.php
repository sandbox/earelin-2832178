<?php

namespace Drupal\spreadsheet_importer\Plugin\SpreadsheetImporter\Formatter;

use Drupal\Core\Form\FormStateInterface;

use Drupal\spreadsheet_importer\Plugin\FormatterBase;

/**
 * Explode formatter plugin.
 *
 * @Formatter(
 *   id = "explode",
 *   label = @Translation("Explode"),
 * )
 */
class Explode extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state) {
    $form = array();

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#default_value' => isset($this->configuration['separator'])
      ? $this->configuration['separator'] : ',',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function format($field_value) {
    $separator = isset($this->configuration['separator']) ? $this->configuration['separator'] : ',';
    $values = explode($separator, $field_value);
    foreach ($values as $key => $value) {
      $values[$key] = trim($value);
    }
    if (count($values) < 1) {
      return trim($field_value);
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function inverseFormat($field_value) {
    $separator = isset($this->configuration['separator']) ? $this->configuration['separator'] : ',';

    if (is_array($field_value)) {
      return implode($separator, $field_value);
    }

    return $field_value;
  }

}
