<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Field plugin interface.
 */
interface FieldPluginInterface {

  /**
   * Get target candidates.
   */
  public function getTargetCandidates($field);

  /**
   * Get configuration form.
   */
  public function getForm(FormStateInterface $form_state, $field_definition);

  /**
   * Import a field.
   *
   * @param array $node_data
   *   Node data array.
   * @param mixed $field_value
   *   Field value.
   */
  public function processField($entity, $field_value);

  /**
   *
   */
  public function exportField($entity);

  /**
   * Returns the guid source field or false if there is no guid source.
   */
  public function getGuidField();

}
