<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for processor plugins.
 */
abstract class ProcessorBase extends PluginBase implements ProcessorPluginInterface {

}
