<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Formater plugin base interface.
 */
interface FormatterPluginInterface {

  /**
   * Get configuration form.
   */
  public function getForm(FormStateInterface $form_state);

  /**
   * Formats a string.
   *
   * @param string $field_value
   *   Field value.
   *
   * @return mixed
   *   Formatted string.
   */
  public function format($field_value);

  /**
   *
   */
  public function inverseFormat($field_value);

}
