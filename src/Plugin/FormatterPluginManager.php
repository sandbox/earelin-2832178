<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages spreadsheet importer formatter plugins.
 */
class FormatterPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SpreadsheetImporter/Formatter',
      $namespaces,
      $module_handler,
      'Drupal\spreadsheet_importer\Plugin\FormatterPluginInterface',
      'Drupal\spreadsheet_importer\Annotation\Formatter'
    );

    $this->alterInfo('spreadsheet_importer_formatter_info');
    $this->setCacheBackend($cache_backend, 'spreadsheet_importer_formatter_plugins');
  }

}
