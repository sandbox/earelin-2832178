<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for field plugins.
 */
abstract class FieldBase extends PluginBase implements FieldPluginInterface {

  /**
   *
   */
  public function getGuidField() {

  }

}
