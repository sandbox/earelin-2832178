<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages spreadsheet importer processor plugins.
 */
class ProcessorPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SpreadsheetImporter/Processor',
      $namespaces,
      $module_handler,
      'Drupal\spreadsheet_importer\Plugin\ProcessorPluginInterface',
      'Drupal\spreadsheet_importer\Annotation\Processor'
    );

    $this->alterInfo('spreadsheet_importer_processor_info');
    $this->setCacheBackend($cache_backend, 'spreadsheet_importer_processor_plugins');
  }

}
