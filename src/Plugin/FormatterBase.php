<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for creating formatters.
 */
abstract class FormatterBase extends PluginBase implements FormatterPluginInterface {

}
