<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages spreadsheet importer field plugins.
 */
class FieldPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SpreadsheetImporter/Field',
      $namespaces,
      $module_handler,
      'Drupal\spreadsheet_importer\Plugin\FieldPluginInterface',
      'Drupal\spreadsheet_importer\Annotation\Field'
    );

    $this->alterInfo('spreadsheet_importer_field_info');
    $this->setCacheBackend($cache_backend, 'spreadsheet_importer_field_plugins');
  }

  /**
   * Returns the plugin for a field type.
   */
  public function getFieldPlugin($field_type) {
    $definitions = $this->getDefinitions();
    $plugins = array();
    foreach ($definitions as $key => $definition) {
      if (in_array($field_type, $definition['fieldTypes'])
            || in_array('*', $definition['fieldTypes'])) {
        $plugins[] = $this->createInstance($key, ['field_type' => $field_type]);
      }
    }
    return $plugins;
  }

}
