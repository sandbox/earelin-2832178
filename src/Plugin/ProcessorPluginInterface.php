<?php

namespace Drupal\spreadsheet_importer\Plugin;

use Drupal\Core\Form\FormStateInterface;

use Drupal\spreadsheet_importer\MappingItem;

/**
 * Defines the processor functions.
 */
interface ProcessorPluginInterface {

  /**
   * Insert one item.
   *
   * @param array $data
   *   Data from the parser.
   * @param array $mapping
   *   Mapping definition.
   */
  public function insertItem(array $data, array $mapping);

  /**
   * Insert one item.
   *
   * @param array $data
   *   Data from the parser.
   * @param array $mapping
   *   Mapping definition.
   */
  public function updateItem($entity, array $data, array $mapping);

  /**
   *
   */
  public function getTargetCandidates();

  /**
   *
   */
  public function getFieldProcessorForTarget($target);

  /**
   *
   * @param mixed $target
   */
  public function getFieldType($target);

  /**
   * Returns plugin configuration form.
   */
  public function getForm(FormStateInterface $form_state);

  /**
   * Returns field definition of a mapping.
   *
   * @param MappingItem $mapping
   *   The mapping object.
   */
  public function getFieldDefinition(MappingItem $mapping);

  /**
   *
   */
  public function exportItem($entity_id, array $mappings);

}
