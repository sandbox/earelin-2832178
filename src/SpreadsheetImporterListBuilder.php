<?php

namespace Drupal\spreadsheet_importer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of image style entities.
 *
 * @see \Drupal\image\Entity\SpreadsheetImporter
 */
class SpreadsheetImporterListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = '';
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $this->getLabel($entity);
    $row['description'] = $entity->get('description');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    $operations['mapping'] = array(
      'title' => t('Mapping'),
      'weight' => 11,
      'url' => $entity->urlInfo('mapping'),
    );

    return $operations;
  }

}
