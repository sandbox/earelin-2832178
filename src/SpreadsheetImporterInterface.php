<?php

namespace Drupal\spreadsheet_importer;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\file\FileInterface;

/**
 * Provides an interface defining a spreadsheet importer entity.
 */
interface SpreadsheetImporterInterface extends ConfigEntityInterface {

  /**
   * Returns the importer name.
   *
   * @return string
   *   The name of the importer.
   */
  public function getName();

  /**
   * Sets the name of the importer.
   *
   * @param string $name
   *   The name of the importer.
   *
   * @return \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   *   The class instance this method is called on.
   */
  public function setName($name);

  /**
   * Returns the importer description.
   *
   * @return string
   *   The description of the importer.
   */
  public function getDescription();

  /**
   * Sets the description of the importer.
   *
   * @param string $description
   *   The description of the importer.
   *
   * @return \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   *   The class instance this method is called on.
   */
  public function setDescription($description);

  /**
   * Returns the importer parser.
   *
   * @return \Drupal\spreadsheet_importer\Plugin\ParserPluginInterface
   *   The parser object.
   */
  public function getParser();

  /**
   * Returns the importer processor.
   *
   * @return \Drupal\spreadsheet_importer\Plugin\ProcessorPluginInterface
   *   The processor object.
   */
  public function getProcessor();

  /**
   * Returns the importer field mapping.
   *
   * @return array
   *   The mapping information.
   */
  public function getMappingItems(): array;

  /**
   * Returns TRUE if source name exists on mapping.
   *
   * @param string $source_name
   *   The name of the source.
   */
  public function sourceExistsInMappings($source_name): bool;

  /**
   * Adds an item to the mapping.
   *
   * @param array $data
   *   The item to be added.
   */
  public function addMappingItem(MappingItem $mapping);

  /**
   * Adds an item to the mapping.
   *
   * @return array
   *   The mapping item.
   */
  public function getMappingItem($mapping_id);

  /**
   * Removes an item from the mapping.
   *
   * @param string $mapping_id
   *   The mapping id.
   */
  public function removeMappingsItem($mapping_id);

  /**
   * Updates an item from the mapping.
   *
   * @param array $data
   *   The mapping data.
   */
  public function updateMappingItem(MappingItem $mapping);

  /**
   * Get the list of candidates for new targets.
   *
   * @return array
   *   An array of target candidates.
   */
  public function getTargetCandidates(): array;

  /**
   * Import items from a file.
   *
   * @param FileInterface $file
   *   Spreadsheet file to import.
   * @param int $limit
   *   Number of rows to import.
   * @param int $offset
   *   First row to import.
   */
  public function import(FileInterface $file, $limit = 0, $offset = 0);

  /**
   * Returns field definition of a mapping.
   *
   * @param int $mapping_id
   *   The mapping id.
   */
  public function getFieldDefinition($mapping_id);

  /**
   *
   */
  public function getGuidMapping();

  /**
   *
   */
  public function hasGuidMapping();

  /**
   *
   */
  public function getMappingItemBySource($source);

  /**
   *
   */
  public function export();

  /**
   *
   */
  public function template();

}
