<?php

namespace Drupal\spreadsheet_importer;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\file\FileInterface;

use Drupal\spreadsheet_importer\Plugin\FormatterPluginManager;

/**
 * @file
 */
class Parser implements ParserInterface {

  /**
   *
   * @var \Drupal\spreadsheet_importer\Plugin\FormatterPluginManager
   */
  protected $formatterPluginManager;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.spreadsheet_importer.formatter')
    );
  }

  /**
   * @param \Drupal\spreadsheet_importer\Plugin\FormatterPluginManager $formatter_plugin_manager
   */
  public function __construct(FormatterPluginManager $formatter_plugin_manager) {
    $this->formatterPluginManager = $formatter_plugin_manager;
  }

  /**
   * Gets CSV header with mapped columns.
   */
  private function getCurrentMappingHeader($mapping, $excel_header) {
    $current_mapping_header = array();

    $mapping_sources = array();
    foreach ($mapping as $map) {
      $mapping_sources[] = $map->getSource();
    }

    foreach ($excel_header->getCellIterator() as $excel_header_cell) {
      if (in_array($excel_header_cell->getFormattedValue(), $mapping_sources)) {
        $current_mapping_header[] = $excel_header_cell->getFormattedValue();
      }
      else {
        $current_mapping_header[] = FALSE;
      }
    }

    return $current_mapping_header;
  }

  /**
   * {@inheritdoc}
   */
  private function applyFormatters($formatters, $value) {
    usort($formatters, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] < $b['weight']) ? 1 : -1;
    });
    foreach ($formatters as $formatter) {
      $formatter_plugin = $this->formatterPluginManager->createInstance($formatter['formatter'], $formatter['formatter_configuration']);
      $value = $formatter_plugin->format($value);
    }
    return $value;
  }

  /**
   *
   */
  private function applyInverseFormatters($formatters, $value) {
    usort($formatters, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] > $b['weight']) ? 1 : -1;
    });
    foreach ($formatters as $formatter) {
      $formatter_plugin = $this->formatterPluginManager->createInstance($formatter['formatter'], $formatter['formatter_configuration']);
      $value = $formatter_plugin->inverseFormat($value);
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function parse(FileInterface $file, SpreadsheetImporterInterface $spreadsheet_importer, $limit = 0, $offset = 0): array {
    $reader = \PHPExcel_IOFactory::createReaderForFile($file->getFileUri());
    $worksheet = $reader->load($file->getFileUri());
    $spreadsheet = $worksheet->getSheet();
    $end_row = $limit == 0 ? NULL : $offset + 1 + $limit;
    $rows_iterator = $spreadsheet->getRowIterator($offset + 1, $end_row);

    $rows = array();

    $i = 0;
    $current_mapping_header = array();
    foreach ($rows_iterator as $spreadsheet_row) {
      if ($i == 0) {
        $current_mapping_header = $this->getCurrentMappingHeader($spreadsheet_importer->getMappingItems(), $spreadsheet_row);
      }
      else {
        $j = 0;
        $row = array();
        foreach ($spreadsheet_row->getCellIterator() as $excel_cell) {
          if ($current_mapping_header[$j]) {
            $mapping_item = $spreadsheet_importer->getMappingItemBySource($current_mapping_header[$j]);
            $row[$current_mapping_header[$j]] = $this->applyFormatters($mapping_item->getFormatters(), $excel_cell->getFormattedValue());
          }
          $j++;
        }
        $rows[] = $row;
      }
      $i++;
    }

    return $rows;
  }

  /**
   *
   */
  public function template(SpreadsheetImporterInterface $spreadsheet_importer, array $row) {
    $directory = 'temporary://';
    $file = file_destination($directory . $spreadsheet_importer->id() . '.xls', FILE_EXISTS_RENAME);
    $spreadsheet = new \PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);
    foreach ($row as $key => $cell) {
      $coordinates = chr(ord('A') + $key) . '1';
      $spreadsheet->getActiveSheet()->setCellValue($coordinates, $cell);
    }

    $writer = \PHPExcel_IOFactory::createWriter($spreadsheet, "Excel5");
    $writer->save($file);
    return $file;
  }

  /**
   *
   */
  public function export(SpreadsheetImporterInterface $spreadsheet_importer, array $data) {
    $directory = 'temporary://';
    $file = file_destination($directory . $spreadsheet_importer->id() . '.xls', FILE_EXISTS_RENAME);

    foreach ($data as $key => &$row) {
      $i = 0;
      foreach ($spreadsheet_importer->getMappingItems() as $mapping) {
        $row[$i] = $this->applyInverseFormatters($mapping->getFormatters(), $row[$i]);
        $i++;
      }
    }

    $spreadsheet = new \PHPExcel();
    $spreadsheet->getActiveSheet()->fromArray($data, NULL, 'A1');

    $writer = \PHPExcel_IOFactory::createWriter($spreadsheet, "Excel5");
    $writer->save($file);
    return $file;
  }

}
