<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;

use Drupal\spreadsheet_importer\Plugin\ProcessorPluginManager;

/**
 * Adding and edit spreadsheet importers form.
 */
class SpreadsheetImporterBaseForm extends EntityForm {

  /**
   * Query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQueryFactory;

  /**
   * Processor plugins manager.
   *
   * @var \Drupal\spreadsheet_importer\Plugin\ProcessorPluginManager
   */
  protected $processorPluginManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query service.
   * @param \Drupal\spreadsheet_importer\Plugin\ProcessorPluginManager $processor_plugin_manager
   *   The processor plugin manager.
   */
  public function __construct(QueryFactory $query_factory,
        ProcessorPluginManager $processor_plugin_manager) {
    $this->entityQueryFactory = $query_factory;
    $this->processorPluginManager = $processor_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('plugin.manager.spreadsheet_importer.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'spreadsheet_importer/admin';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t("Importer label."),
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
      ),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getDescription(),
      '#description' => $this->t("Importer description."),
    ];

    $form['insert_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert items'),
      '#default_value' => $this->entity->isNew() ? TRUE : $this->entity->get('insert_items'),
    ];

    $form['update_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update items'),
      '#default_value' => $this->entity->isNew() ? FALSE : $this->entity->get('update_items'),
    ];

    $form['plugins_settings'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    ];

    $form['processor_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Processor'),
      '#group' => 'plugins_settings',
    ];

    // Load the list of processors.
    $processor_options = array();
    $processor_plugins = $this->processorPluginManager->getDefinitions();
    uasort($processor_plugins, function ($a, $b) {
      return strcasecmp($a['label'], $b['label']);
    });
    foreach ($processor_plugins as $processor_plugin => $definition) {
      $processor_options[$processor_plugin] = $definition['label'];
    }

    $form['processor_wrapper']['processor'] = [
      '#type' => 'select',
      '#title' => $this->t('Processor'),
      '#options' => $processor_options,
      '#default_value' => $this->entity->get('processor'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changeProcessorCallback',
        'wrapper' => 'processor-configuration',
      ],
    ];

    $form['processor_wrapper']['processor_configuration'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'processor-configuration'],
      '#tree' => TRUE,
    ];

    if ($this->entity->get('processor')) {
      $processor_configuration = $this->entity->get('processor_configuration') !== NULL ? $this->entity->get('processor_configuration') : array();
      $processor_instance = $this->processorPluginManager->createInstance($this->entity->get('processor'), $processor_configuration);
      $form['processor_wrapper']['processor_configuration'] = array_merge($form['processor_wrapper']['processor_configuration'], $processor_instance->getForm($form_state));
    }

    return parent::form($form, $form_state);
  }

  /**
   * Implements callback for Ajax event on processor change.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   *   Plugin form.
   */
  public function changeProcessorCallback(array &$form, FormStateInterface $form_state) {
    return $form['processor_wrapper']['processor_configuration'];
  }

  /**
   * Checks for an existing spreadsheet importer.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    $query = $this->entityQueryFactory->get('spreadsheet_importer');
    $result = $query->condition('name', $entity_id)
      ->execute();
    return (bool) $result;
  }

}
