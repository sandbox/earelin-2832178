<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Add importer form.
 */
class SpreadsheetImporterAddForm extends SpreadsheetImporterBaseForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.spreadsheet_importer.mapping', [
      'spreadsheet_importer' => $this->entity->id(),
    ]);
    parent::buildForm($form, $form_state);
  }

}
