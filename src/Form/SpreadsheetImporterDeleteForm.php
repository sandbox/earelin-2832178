<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Delete spreadsheet importers form.
 */
class SpreadsheetImporterDeleteForm extends EntityDeleteForm {

}
