<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\Entity\SpreadsheetImporter;

/**
 * Delete imported items form.
 */
class DeleteImportedItemsForm extends ConfirmFormBase {

  /**
   * The spreadsheet importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  protected $database;

  protected $entityTypeManager;

  /**
   * Checks form access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access(AccountInterface $account, $spreadsheet_importer) {
    if ($account->hasPermission('spreadsheet importer delete ' . $spreadsheet_importer)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->database = Database::getConnection();
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('spreadsheet_importer.importers.import', [
      'spreadsheet_importer' => $this->spreadsheetImporter->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_import_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): string {
    return $this->t("Are you sure you want to delete imported items from @importer?", ['@importer' => $this->spreadsheetImporter->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporter $spreadsheet_importer = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;

    $query = $this->database->select('spreadsheet_importer', 'si');
    $query->addField('si', 'guid');
    $imported_items = $query->condition('importer', $this->spreadsheetImporter->id())
      ->execute()
      ->fetchAll();
    $imported_count = count($imported_items);

    if ($imported_count == 0) {
      $form['imported_items'] = [
        '#type' => 'markup',
        '#markup' => '<p>No imported items.</p>',
      ];
      return $form;
    }

    $form['imported_items'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>@count imported items will be deleted.</p>', ['@count' => $imported_count]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns page title.
   */
  public function getTitle(SpreadsheetImporter $spreadsheet_importer) {
    return $spreadsheet_importer->label();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $importer_id = $this->spreadsheetImporter->id();

    $query = $this->database->select('spreadsheet_importer', 'si');
    $imported_items = $query->fields('si', array('guid', 'entity_type', 'entity_id'))
      ->condition('importer', $importer_id)
      ->execute()
      ->fetchAll();

    $deleted = 0;
    foreach ($imported_items as $imported_item) {
      $entity = $this->entityTypeManager->getStorage($imported_item->entity_type)->load($imported_item->entity_id);
      $entity->delete();
      $this->database->delete('spreadsheet_importer')
        ->condition('guid', $imported_item->guid)
        ->execute();
      $deleted++;
    }

    drupal_set_message($this->t("@number items deleted.", array('@number' => $deleted)));

    $form_state->setRedirectUrl(Url::fromRoute('spreadsheet_importer.importers.import',
                  ['spreadsheet_importer' => $this->spreadsheetImporter->id()]));
  }

}
