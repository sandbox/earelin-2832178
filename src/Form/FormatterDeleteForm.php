<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\Entity\SpreadsheetImporter;

/**
 * Delete formatter form.
 */
class FormatterDeleteForm extends ConfirmFormBase {

  /**
   * The spreadsheet importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  /**
   * The mapping item.
   *
   * @var \Drupal\spreadsheet_importer\MappingItem
   */
  protected $mappingItem;

  /**
   * The formatter id to be removed.
   *
   * @var string
   */
  protected $formatterId;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporter $spreadsheet_importer = NULL, $mapping_id = NULL, $formatter_id = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;
    $this->mappingItem = $this->spreadsheetImporter->getMappingItem($mapping_id);
    $this->formatterId = $formatter_id;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('spreadsheet_importer.mapping.formatters', [
      'spreadsheet_importer' => $this->spreadsheetImporter->id(),
      'mapping_id' => $this->mappingItem->getId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_formatter_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): string {
    return $this->t("Are you sure you want to delete the mapping column @column_name?", ['@column_name' => $this->mappingItem->getSource()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->mappingItem->removeFormatter($this->formatterId);
    $this->spreadsheetImporter->updateMappingItem($this->mappingItem);
    $this->spreadsheetImporter->save();
    $form_state->setRedirectUrl(Url::fromRoute('spreadsheet_importer.mapping.formatters',
                  [
                    'spreadsheet_importer' => $this->spreadsheetImporter->id(),
                    'mapping_id' => $this->mappingItem->getId(),
                  ]));
  }

}
