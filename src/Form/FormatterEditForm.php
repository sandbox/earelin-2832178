<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\Entity\SpreadsheetImporter;
use Drupal\spreadsheet_importer\Plugin\FormatterPluginManager;

/**
 * Provides a form for editing a formatter instance.
 */
class FormatterEditForm extends FormBase {

  /**
   * Importers field plugins manager.
   *
   * @var \Drupal\spreadsheet_importer\Plugin\FormatterPluginManager
   */
  protected $formatterPluginManager;

  protected $mappingItem;

  protected $formatter;

  protected $spreadsheetImporter;

  /**
   * Class constructor.
   */
  public function __construct(FormatterPluginManager $formatter_plugin_manager) {
    $this->formatterPluginManager = $formatter_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.spreadsheet_importer.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporter $spreadsheet_importer = NULL, $mapping_id = NULL, $formatter_id = NULL): array {
    $this->spreadsheetImporter = $spreadsheet_importer;
    $this->mappingItem = $spreadsheet_importer->getMappingItem($mapping_id);
    $this->formatter = $this->mappingItem->getFormatter($formatter_id);
    $formatter_instance = $this->formatterPluginManager->createInstance($this->formatter['formatter'], $this->formatter['formatter_configuration']);

    $plugin_form = $formatter_instance->getForm($form_state);
    $form['plugin_keys'] = [
      '#type' => 'value',
      '#value' => array_keys($plugin_form),
    ];

    $form = array_merge($form, $plugin_form);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_formatter_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin_keys = $form_state->getValue('plugin_keys');
    foreach ($plugin_keys as $key) {
      $this->formatter['formatter_configuration'][$key] = $form_state->getValue($key);
    }
    $this->mappingItem->updateFormatter($this->formatter);
    $this->spreadsheetImporter->updateMappingItem($this->mappingItem);
    $this->spreadsheetImporter->save();
    $form_state->setRedirectUrl(Url::fromRoute('spreadsheet_importer.mapping.formatters',
                  [
                    'spreadsheet_importer' => $this->spreadsheetImporter->id(),
                    'mapping_id' => $this->mappingItem->getId(),
                  ]));
  }

}
