<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Provides a form for importing a file.
 */
class ImportForm extends FormBase {

  protected $spreadsheetImporter;

  protected $fileStorage;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Checks form access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access(AccountInterface $account, $spreadsheet_importer) {
    if ($account->hasPermission('spreadsheet importer import ' . $spreadsheet_importer)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importers_import_form';
  }

  /**
   * Returns form title.
   *
   * @param SpreadsheetImporterInterface $spreadsheet_importer
   *   The importer.
   *
   * @return string
   *   Page title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer) {
    return $this->t('Import @title', array('@title' => $spreadsheet_importer->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporterInterface $spreadsheet_importer = NULL): array {
    $this->spreadsheetImporter = $spreadsheet_importer;

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->spreadsheetImporter->getDescription(),
    ];

    $form['download_template'] = [
      '#type' => 'link',
      '#title' => $this->t('Download tempate'),
      '#url' => Url::fromRoute('spreadsheet_importer.importers.template', [
        'spreadsheet_importer' => $this->spreadsheetImporter->id(),
      ]),
      '#prefix' => '<p>',
      '#sufix' => '</p>',
    ];

    if (\Drupal::currentUser()->hasPermission('spreadsheet importer download ' . $this->spreadsheetImporter->id())) {
      $form['download_uploaded_data'] = [
        '#type' => 'link',
        '#title' => $this->t('Download uploaded data'),
        '#url' => Url::fromRoute('spreadsheet_importer.importers.export', [
          'spreadsheet_importer' => $this->spreadsheetImporter->id(),
        ]),
        '#prefix' => '<p>',
        '#sufix' => '</p>',
      ];
    }

    $form['file'] = [
      '#type' => 'managed_file',
      '#upload_validators' => array(
        'file_validate_extensions' => array('csv xls'),
      ),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fid = $form_state->getValue('file');
    $file = $this->fileStorage->load($fid[0]);
    $result = $this->spreadsheetImporter->import($file);
    drupal_set_message($this->t('@number items processed', array('@number' => $result['total'])));
    if ($result['imported']) {
      drupal_set_message($this->t('@number items imported', array('@number' => $result['import'])));
    }
    if ($result['updated']) {
      drupal_set_message($this->t('@number items updated', array('@number' => $result['updated'])));
    }
  }

}
