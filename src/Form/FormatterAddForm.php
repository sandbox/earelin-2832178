<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\Plugin\FormatterPluginManager;
use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Provides a form for creating formatter instance.
 */
class FormatterAddForm extends FormBase {

  /**
   * The spreadsheet importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  /**
   * Importers field plugins manager.
   *
   * @var \Drupal\spreadsheet_importer\Plugin\FormatterPluginManager
   */
  protected $formatterPluginManager;

  /**
   * Class constructor.
   */
  public function __construct(FormatterPluginManager $formatter_plugin_manager) {
    $this->formatterPluginManager = $formatter_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.spreadsheet_importer.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_formatter_add_form';
  }

  /**
   * Returns form title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer, $mapping_id) {
    return $this->t('Add mapping item to @label', array('@label' => $spreadsheet_importer->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporterInterface $spreadsheet_importer = NULL, $mapping_id = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;
    $definitions = $this->formatterPluginManager->getDefinitions();

    uasort($definitions, function ($a, $b) {
      return strcasecmp($a['label'], $b['label']);
    });
    $formatter_options = array();
    foreach ($definitions as $formatter_plugin => $definition) {
      $formatter_options[$formatter_plugin] = $definition['label'];
    }

    $form['mapping_id'] = [
      '#type' => 'value',
      '#value' => $mapping_id,
    ];

    $form['formatter'] = [
      '#type' => 'select',
      '#title' => $this->t('Formatter'),
      '#options' => $formatter_options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changeFormatterCallback',
        'wrapper' => 'formatter-configuration',
      ],
    ];

    $form['formatter_wrapper']['formatter_configuration'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'formatter-configuration'],
      '#tree' => TRUE,
    ];

    $values = $form_state->getValues();

    if (isset($values['formatter'])) {
      $formatter_configuration = isset($values['formatter_configuration']) ? $values['formatter_configuration'] : array();
      $formatter_instance = $this->formatterPluginManager->createInstance($values['formatter'], $formatter_configuration);
      $form['formatter_wrapper']['formatter_configuration'] = array_merge($form['formatter_wrapper']['formatter_configuration'], $formatter_instance->getForm($form_state));
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mapping = $this->spreadsheetImporter->getMappingItem($form_state->getValue('mapping_id'));
    $values = $form_state->getValues();
    $mapping->addFormatter(array(
      'formatter' => $values['formatter'],
      'formatter_configuration' => $values['formatter_configuration'],
    ));
    $this->spreadsheetImporter->updateMappingItem($mapping);
    $this->spreadsheetImporter->save();

    $form_state->setRedirectUrl(Url::fromRoute('spreadsheet_importer.mapping.formatters',
                  ['spreadsheet_importer' => $this->spreadsheetImporter->id(), 'mapping_id' => $mapping->getId()]));
  }

  /**
   * Implements callback for Ajax event on processor change.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   *   Plugin form.
   */
  public function changeFormatterCallback(array &$form, FormStateInterface $form_state) {
    return $form['formatter_wrapper']['formatter_configuration'];
  }

}
