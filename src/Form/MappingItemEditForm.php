<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;
use Drupal\spreadsheet_importer\Plugin\FieldPluginManager;

/**
 * Provides a form for mapping settings.
 */
class MappingItemEditForm extends FormBase {

  /**
   * The importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  /**
   * Mapping item to edit.
   *
   * @var \Drupal\spreadsheet_importer\MappingItem
   */
  protected $mappingItem;

  /**
   * Field plugin for the mapping.
   *
   * @var \Drupal\spreadsheet_importer\Plugin\FieldPluginInterface
   */
  protected $fieldPlugin;

  /**
   * Importers field plugins manager.
   *
   * @var \Drupal\spreadsheet_importer\Plugin\FieldPluginManager
   */
  protected $fieldPluginManager;

  /**
   * Class constructor.
   *
   * @param FieldPluginManager $field_plugin_manager
   *   The importer field plugin manager.
   */
  public function __construct(FieldPluginManager $field_plugin_manager) {
    $this->fieldPluginManager = $field_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.spreadsheet_importer.field')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_mapping_item_edit_form';
  }

  /**
   * Returns form title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer, $mapping_id) {
    $mapping_item = $spreadsheet_importer->getMappingItem($mapping_id);
    return $this->t('Edit @source mapping', array('@source' => $mapping_item->getSource()));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporterInterface $spreadsheet_importer = NULL, $mapping_id = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;
    $this->mappingItem = $this->spreadsheetImporter->getMappingItem($mapping_id);

    $this->fieldPlugin = $this->fieldPluginManager->createInstance($this->mappingItem->getField(), $this->mappingItem->getConfiguration());

    $field_definition = $this->spreadsheetImporter->getFieldDefinition($mapping_id);
    $guid_mapping = $this->spreadsheetImporter->getGuidMapping();

    $form['id'] = [
      '#type' => 'value',
      '#value' => $mapping_id,
    ];

    $form['source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spreadsheet column'),
      '#default_value' => $this->mappingItem->getSource(),
      '#required' => TRUE,
    ];

    $form['target'] = [
      '#type' => 'markup',
      '#markup' => '<strong>' . $this->t('Target') . '</strong><br/>' . $this->mappingItem->getTarget() . ':' . $this->mappingItem->getSubTarget(),
    ];

    $form['guid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use as id.'),
      '#default_value' => $guid_mapping->getId() == $mapping_id,
      '#disabled' => $this->spreadsheetImporter->hasGuidMapping() && $guid_mapping->getId() != $mapping_id,
    ];

    $form['field'] = [
      '#type' => 'value',
      '#value' => $this->fieldPlugin->getPluginId(),
    ];

    $form['field_configuration'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['field_configuration'] = array_merge($form['field_configuration'], $this->fieldPlugin->getForm($form_state, $field_definition));

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->mappingItem->setSource($form_state->getValue('source'));
    $this->mappingItem->setGuid($form_state->getValue('guid'));
    $this->mappingItem->setFieldConfiguration($form_state->getValue('field_configuration'));
    $this->spreadsheetImporter->updateMappingItem($this->mappingItem);
    $this->spreadsheetImporter->save();
    $form_state->setRedirect('entity.spreadsheet_importer.mapping', [
      'spreadsheet_importer' => $this->spreadsheetImporter->id(),
    ]);
  }

}
