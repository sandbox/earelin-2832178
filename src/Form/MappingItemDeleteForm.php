<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\Entity\SpreadsheetImporter;
use Drupal\spreadsheet_importer\MappingItem;

/**
 * Delete mapping item form.
 */
class MappingItemDeleteForm extends ConfirmFormBase {

  /**
   * The spreadsheet importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  /**
   * The mapping item to be deleted.
   *
   * @var \Drupal\spreadsheet_importer\MappingItem
   */
  protected $mappingItem;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporter $spreadsheet_importer = NULL, $mapping_id = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;
    $this->mappingItem = $this->spreadsheetImporter->getMappingItem($mapping_id);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('entity.spreadsheet_importer.mapping', ['spreadsheet_importer' => $this->spreadsheetImporter->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_mapping_item_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): string {
    return $this->t("Are you sure you want to delete the mapping column @column_name?", ['@column_name' => $this->mappingItem->getSource()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->spreadsheetImporter->removeMappingsItem($this->mappingItem->getId());
    $this->spreadsheetImporter->save();
    $form_state->setRedirectUrl(Url::fromRoute('entity.spreadsheet_importer.mapping',
                  ['spreadsheet_importer' => $this->spreadsheetImporter->id()]));
  }

}
