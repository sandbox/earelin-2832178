<?php

namespace Drupal\spreadsheet_importer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Transliteration\PhpTransliteration;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\MappingItem;
use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Provides a form for create a mapping item.
 */
class MappingItemAddForm extends FormBase {

  /**
   * The spreadsheet importer.
   *
   * @var \Drupal\spreadsheet_importer\SpreadsheetImporterInterface
   */
  protected $spreadsheetImporter;

  /**
   * Transliteration service.
   *
   * @var \Drupal\Core\Transliteration\PhpTransliteration
   */
  protected $transliteration;

  /**
   * Class constructor.
   */
  public function __construct(PhpTransliteration $transliteration) {
    $this->transliteration = $transliteration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('transliteration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'spreadsheet_importer_mapping_item_add_form';
  }

  /**
   * Returns form title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer) {
    return $this->t('Add mapping item to @label', array('@label' => $spreadsheet_importer->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SpreadsheetImporterInterface $spreadsheet_importer = NULL) {
    $this->spreadsheetImporter = $spreadsheet_importer;

    $target_cadidates = $this->spreadsheetImporter->getTargetCandidates();

    $form['source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spreadsheet column'),
      '#required' => TRUE,
    ];

    $form['target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target'),
      '#options' => $target_cadidates,
      '#required' => TRUE,
    ];

    $form['guid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use as id.'),
      '#default_value' => FALSE,
      '#disabled' => $this->spreadsheetImporter->hasGuidMapping(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->spreadsheetImporter->sourceExistsInMappings($form_state->getValue('source'))) {
      $form_state->setErrorByName('source', $this->t('The source name is been used.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $transliterated_name = $this->transliteration->transliterate($form_state->getValue('source'));
    $machine_name = str_replace(" ", "_", strtolower($transliterated_name));
    // $field_processor = $this->spreadsheetImporter->getProcessor()->getFieldProcessorForTarget($form_state->getValue('target'));
    $field_mapping = explode(';', $form_state->getValue('target'));
    $targets = explode(':', $field_mapping[0]);
    $this->spreadsheetImporter->addMappingItem(new MappingItem([
      'id' => $machine_name,
      'source' => $form_state->getValue('source'),
      'target' => $targets[0],
      'subtarget' => isset($targets[1]) ? $targets[1] : NULL,
      'guid' => $form_state->getValue('guid'),
      'weight' => 0,
      'field' => $field_mapping[1],
      'field_type' => $field_mapping[2],
    ]));
    $this->spreadsheetImporter->save();

    $form_state->setRedirectUrl(Url::fromRoute('spreadsheet_importer.mapping.formatters',
                  ['spreadsheet_importer' => $this->spreadsheetImporter->id(), 'mapping_id' => $machine_name]));
  }

}
