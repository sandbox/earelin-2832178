<?php

namespace Drupal\spreadsheet_importer\Form;

use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Importer edit form.
 */
class SpreadsheetImporterEditForm extends SpreadsheetImporterBaseForm {

  /**
   * Returns page title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer) {
    return $spreadsheet_importer->label();
  }

}
