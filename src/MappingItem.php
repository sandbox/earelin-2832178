<?php

namespace Drupal\spreadsheet_importer;

/**
 *
 */
class MappingItem implements MappingItemInterface {

  protected $configuration;

  /**
   *
   */
  private function generateUniqueId($base) {
    $ids = array_column($this->configuration['formatters'], 'id');

    do {
      $match = FALSE;
      if (in_array($base, $ids)) {
        $match = TRUE;
        $matches = array();
        if (preg_match('/^(.*)_([0-9]*)?/', $base, $matches)) {
          $base = $matches[1] . '_' . (++$matches[2]);
        }
        else {
          $base = $base . '_1';
        }
      }
    } while ($match);

    return $base;
  }

  /**
   * Class constructor.
   */
  public function __construct(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   *
   */
  public function addFormatter(array $formatter_configuration) {
    $id = $this->generateUniqueId($formatter_configuration['formatter']);
    $formatter_configuration['id'] = $id;
    $formatter_configuration['weight'] = 0;
    $this->configuration['formatters'][] = $formatter_configuration;
  }

  /**
   *
   */
  public function getFormatter($formatter_id) {
    if (isset($this->configuration['formatters'])) {
      foreach ($this->configuration['formatters'] as $formatter) {
        if ($formatter['id'] == $formatter_id) {
          return $formatter;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  public function removeFormatter($formatter_id) {
    if (isset($this->configuration['formatters'])) {
      foreach ($this->configuration['formatters'] as $key => $formatter) {
        if ($formatter['id'] == $formatter_id) {
          unset($this->configuration['formatters'][$key]);
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  public function updateFormatter(array $formatter_configuration) {
    if (isset($this->configuration['formatters'])) {
      foreach ($this->configuration['formatters'] as $key => $formatter) {
        if ($formatter['id'] == $formatter_configuration['id']) {
          $this->configuration['formatters'][$key] = $formatter_configuration;
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  public function getId() {
    return $this->configuration['id'];
  }

  /**
   *
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   *
   */
  public function getSource() {
    return $this->configuration['source'];
  }

  /**
   *
   */
  public function getTarget() {
    return $this->configuration['target'];
  }

  /**
   *
   */
  public function getFormatters() {
    if (isset($this->configuration['formatters'])) {
      return $this->configuration['formatters'];
    }
    return array();
  }

  /**
   *
   */
  public function getField() {
    return $this->configuration['field'];
  }

  /**
   *
   */
  public function getFieldConfiguration() {
    return $this->configuration['field_configuration'];
  }

  /**
   *
   */
  public function setField($field) {
    $this->configuration['field'] = $field;
  }

  /**
   *
   */
  public function setFieldConfiguration($field_configuration) {
    $this->configuration['field_configuration'] = $field_configuration;
  }

  /**
   *
   */
  public function setSource($source) {
    $this->configuration['source'] = $source;
  }

  /**
   *
   */
  public function setTarget($target) {
    $this->configuration['target'] = $target;
  }

  /**
   *
   */
  public function getWeight() {
    return $this->configuration['weight'];
  }

  /**
   *
   */
  public function setWeight($weight) {
    $this->configuration['weight'] = $weight;
  }

  /**
   *
   */
  public function isGuid() {
    return $this->configuration['guid'];
  }

  /**
   *
   */
  public function setGuid($value) {
    $this->configuration['guid'] = $value;
  }

  /**
   *
   */
  public function getSubTarget() {
    return $this->configuration['subtarget'];
  }

  /**
   *
   */
  public function setSubTarget($subtarget) {
    $this->configuration['subtarget'] = $subtarget;
  }

}
