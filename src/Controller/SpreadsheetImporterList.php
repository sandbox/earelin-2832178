<?php

namespace Drupal\spreadsheet_importer\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Url;

/**
 * Lists the importers.
 */
class SpreadsheetImporterList extends ControllerBase {

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQueryFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query.
   */
  public function __construct(QueryFactory $query_factory) {
    $this->entityQueryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * Loads the list of spreadsheet importers.
   *
   * @return array
   *   Importers data.
   */
  private function getSpreadsheetImportersList() {
    $query = $this->entityQueryFactory->get('spreadsheet_importer');
    $spreadsheet_importers_ids = $query->sort('name')
      ->execute();
    $spreadsheet_importers_storage = $this->entityTypeManager()->getStorage('spreadsheet_importer');
    return $spreadsheet_importers_storage->loadMultiple($spreadsheet_importers_ids);
  }

  /**
   * Lists the importers.
   */
  public function listItems() {

    $render = [];

    $render['list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Import'),
        $this->t('Description'),
      ],
    ];

    foreach ($this->getSpreadsheetImportersList() as $importer) {
      $subrender = [];

      $subrender['name'] = [
        '#type' => 'link',
        '#title' => $importer->label(),
        '#url' => Url::fromRoute('spreadsheet_importer.importers.import',
                  ['spreadsheet_importer' => $importer->id()]),
      ];

      $subrender['description'] = [
        '#type' => 'markup',
        '#markup' => $importer->getDescription(),
      ];

      $render['list'][] = $subrender;
    }

    return $render;
  }

}
