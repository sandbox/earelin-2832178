<?php

namespace Drupal\spreadsheet_importer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Lists the mapping items belonging to a spreadsheet importer.
 */
class MappingItemsController extends ControllerBase {

  /**
   * Returns mapping page title.
   *
   * @param SpreadsheetImporterInterface $spreadsheet_importer
   *   The importer.
   *
   * @return string
   *   Page title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer) {
    return $this->t('%label mapping', array('%label' => $spreadsheet_importer->label()));
  }

  /**
   * Lists the feed items belonging to a feed.
   */
  public function listItems(SpreadsheetImporterInterface $spreadsheet_importer) {
    $render = array();

    $render['list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Spreadsheet column'),
        $this->t('Entity field'),
        $this->t('Operations'),
      ],
    ];

    foreach ($spreadsheet_importer->getMappingItems() as $mapping) {
      $row_render = array();

      $row_render['spreadsheet'] = [
        '#markup' => $mapping->getSource(),
      ];

      $row_render['entity'] = [
        '#markup' => $mapping->getTarget(),
      ];

      $row_render['operations'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('spreadsheet_importer.mapping.edit_form',
                  ['spreadsheet_importer' => $spreadsheet_importer->id(), 'mapping_id' => $mapping->getId()]),
          ],
          'formatters' => [
            'title' => $this->t('Formatters'),
            'url' => Url::fromRoute('spreadsheet_importer.mapping.formatters',
                  ['spreadsheet_importer' => $spreadsheet_importer->id(), 'mapping_id' => $mapping->getId()]),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('spreadsheet_importer.mapping.delete_form',
                  ['spreadsheet_importer' => $spreadsheet_importer->id(), 'mapping_id' => $mapping->getId()]),
          ],
        ],
      ];

      $render['list'][] = $row_render;
    }

    return $render;
  }

}
