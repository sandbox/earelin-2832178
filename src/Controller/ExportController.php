<?php

namespace Drupal\spreadsheet_importer\Controller;

use Symfony\Component\HttpFoundation\Response;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;

use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Lists the mapping items belonging to a spreadsheet importer.
 */
class ExportController extends ControllerBase {

  /**
   * Checks form access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function exportAccess(AccountInterface $account, $spreadsheet_importer) {
    if ($account->hasPermission('spreadsheet importer download ' . $spreadsheet_importer)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Lists the feed items belonging to a feed.
   */
  public function export(SpreadsheetImporterInterface $spreadsheet_importer) {
    $file = $spreadsheet_importer->export();

    // Generate response.
    $response = new Response();

    // Set headers.
    $response->headers->set('Cache-Control', 'private');
    $response->headers->set('Content-type', mime_content_type($file));
    $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($file) . '";');
    $response->headers->set('Content-length', filesize($file));

    // Send headers before outputting anything.
    $response->sendHeaders();

    $response->setContent(file_get_contents($file));

    return $response;
  }

  /**
   *
   */
  public function template(SpreadsheetImporterInterface $spreadsheet_importer) {
    $file = $spreadsheet_importer->template();

    // Generate response.
    $response = new Response();

    // Set headers.
    $response->headers->set('Cache-Control', 'private');
    $response->headers->set('Content-type', mime_content_type($file));
    $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($file) . '";');
    $response->headers->set('Content-length', filesize($file));

    // Send headers before outputting anything.
    $response->sendHeaders();

    $response->setContent(file_get_contents($file));

    return $response;
  }

}
