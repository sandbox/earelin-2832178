<?php

namespace Drupal\spreadsheet_importer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 *
 */
class FormatterItemsController extends ControllerBase {

  /**
   * Returns mapping formatters page title.
   *
   * @param SpreadsheetImporterInterface $spreadsheet_importer
   *   The importer.
   *
   * @return string
   *   Page title.
   */
  public function getTitle(SpreadsheetImporterInterface $spreadsheet_importer, $mapping_id) {
    $mapping = $spreadsheet_importer->getMappingItem($mapping_id);
    return $this->t('%label formatters', array('%label' => $mapping->getSource()));
  }

  /**
   * Lists the feed items belonging to a feed.
   */
  public function listItems(SpreadsheetImporterInterface $spreadsheet_importer, $mapping_id) {
    $mapping = $spreadsheet_importer->getMappingItem($mapping_id);

    $render = array();

    $render['list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Formatter'),
        $this->t('Operations'),
      ],
    ];

    $formatters = $mapping->getFormatters();
    usort($formatters, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] < $b['weight']) ? 1 : -1;
    });
    foreach ($formatters as $formatter) {
      $row_render = array();

      $row_render['formatter'] = [
        '#markup' => $formatter['formatter'],
      ];

      $row_render['operations'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('spreadsheet_importer.mapping.formatters.edit_form', [
              'spreadsheet_importer' => $spreadsheet_importer->id(),
              'mapping_id' => $mapping->getId(),
              'formatter_id' => $formatter['id'],
            ]),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('spreadsheet_importer.mapping.formatters.delete_form', [
              'spreadsheet_importer' => $spreadsheet_importer->id(),
              'mapping_id' => $mapping->getId(),
              'formatter_id' => $formatter['id'],
            ]),
          ],
        ],
      ];

      $render['list'][] = $row_render;
    }

    return $render;
  }

}
