<?php

namespace Drupal\spreadsheet_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for importer formatter plugins.
 *
 * @Annotation
 */
class Formatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the parser.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
