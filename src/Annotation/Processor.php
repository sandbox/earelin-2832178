<?php

namespace Drupal\spreadsheet_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for importer processor plugins.
 *
 * @Annotation
 */
class Processor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the processor.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A brief description of the processor.
   *
   * @var \Drupal\Core\Annotation\Translationoptional
   */
  public $description = '';

}
