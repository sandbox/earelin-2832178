<?php

namespace Drupal\spreadsheet_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for importer field plugins.
 *
 * @Annotation
 */
class Field extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the parser.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Supported field types.
   *
   * @var array
   */
  public $fieldTypes;

  /**
   * External mapping.
   *
   * @var array
   */
  public $external;

}
