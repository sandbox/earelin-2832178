<?php

namespace Drupal\spreadsheet_importer;

/**
 *
 */
interface MappingItemInterface {

  /**
   *
   */
  public function addFormatter(array $formatter_configuration);

  /**
   *
   */
  public function getFormatter($formatter_id);

  /**
   *
   */
  public function removeFormatter($formater_id);

  /**
   *
   */
  public function updateFormatter(array $formatter_configuration);

  /**
   *
   */
  public function getId();

  /**
   *
   */
  public function getConfiguration();

  /**
   *
   */
  public function getSource();

  /**
   *
   */
  public function setSource($source);

  /**
   *
   */
  public function getTarget();

  /**
   *
   */
  public function setTarget($target);

  /**
   *
   */
  public function getFormatters();

  /**
   *
   */
  public function getField();

  /**
   *
   */
  public function setField($field);

  /**
   *
   */
  public function getFieldConfiguration();

  /**
   *
   */
  public function setFieldConfiguration($field_configuration);

  /**
   *
   */
  public function getWeight();

  /**
   *
   */
  public function setWeight($weight);

  /**
   *
   */
  public function isGuid();

  /**
   *
   */
  public function setGuid($value);

  /**
   *
   */
  public function getSubTarget();

  /**
   *
   */
  public function setSubTarget($sub_target);

}
