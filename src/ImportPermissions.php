<?php

namespace Drupal\spreadsheet_importer;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 *
 */
class ImportPermissions {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQueryFactory;

  /**
   *
   */
  private function getEntityQueryFactory() {
    if (!isset($this->entityQueryFactory)) {
      $this->entityQueryFactory = \Drupal::entityQuery('spreadsheet_importer');
    }
    return $this->entityQueryFactory;
  }

  /**
   *
   */
  private function getEntityTypeManager() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The node type permissions.
   */
  public function getPermissions() {
    $permissions = [];

    $query = $this->getEntityQueryFactory();
    $spreadsheet_importers_ids = $query->sort('name')
      ->execute();
    $spreadsheet_importers_storage = $this->getEntityTypeManager()->getStorage('spreadsheet_importer');
    $spreadsheets = $spreadsheet_importers_storage->loadMultiple($spreadsheet_importers_ids);

    foreach ($spreadsheets as $spreadsheet) {
      $permissions['spreadsheet importer import ' . $spreadsheet->id()] = [
        'title' => $this->t('%importer: import', ['%importer' => $spreadsheet->label()]),
        'description' => $this->t('Import items with %importer importer', ['%importer' => $spreadsheet->label()]),
      ];
      $permissions['spreadsheet importer delete ' . $spreadsheet->id()] = [
        'title' => $this->t('%importer: delete', ['%importer' => $spreadsheet->label()]),
        'description' => $this->t('Delete imported items with %importer importer', ['%importer' => $spreadsheet->label()]),
      ];
      $permissions['spreadsheet importer download ' . $spreadsheet->id()] = [
        'title' => $this->t('%importer: download', ['%importer' => $spreadsheet->label()]),
        'description' => $this->t('Download imported items with %importer importer', ['%importer' => $spreadsheet->label()]),
      ];
    }

    return $permissions;
  }

}
