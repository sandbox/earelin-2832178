<?php

namespace Drupal\spreadsheet_importer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Database\Database;
use Drupal\file\FileInterface;

use Drupal\spreadsheet_importer\MappingItem;
use Drupal\spreadsheet_importer\SpreadsheetImporterInterface;

/**
 * Defines an spreadsheet importer configuration entity.
 *
 * @ConfigEntityType(
 *   id = "spreadsheet_importer",
 *   label = @Translation("Spreadsheet importer"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\spreadsheet_importer\Form\SpreadsheetImporterAddForm",
 *       "edit" = "Drupal\spreadsheet_importer\Form\SpreadsheetImporterEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\spreadsheet_importer\SpreadsheetImporterListBuilder",
 *   },
 *   admin_permission = "administer spreadsheet importers",
 *   config_prefix = "spreadsheet_importer",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/spreadsheet-importers/{spreadsheet_importer}",
 *     "mapping" = "/admin/structure/spreadsheet-importers/{spreadsheet_importer}/mapping",
 *     "delete-form" = "/admin/structure/spreadsheet-importers/{spreadsheet_importer}/delete",
 *     "collection" = "/admin/structure/spreadsheet-importers",
 *   },
 * )
 */
class SpreadsheetImporter extends ConfigEntityBase implements SpreadsheetImporterInterface {

  /**
   * The importer parser.
   *
   * @var SpreadsheetParserInterface
   */
  protected $parser;

  /**
   * The importer processor.
   *
   * @var ProcessorInterface
   */
  protected $processorObject;

  /**
   * The uuid generation service.
   */
  protected $uuidService;

  /**
   * Database connection service.
   */
  protected $database;

  /**
   * The importer mapping.
   *
   * @var array
   */
  protected $mappings = array();

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description');
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParser() {
    if (!isset($this->parser)) {
      $this->parser = \Drupal::service('spreadsheet_importer.parser');
    }
    return $this->parser;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessor() {
    if (!isset($this->processorObject)) {
      $processor_plugin_manager = \Drupal::service('plugin.manager.spreadsheet_importer.processor');
      $this->processorObject = $processor_plugin_manager->createInstance($this->get('processor'), $this->get('processor_configuration'));
    }
    return $this->processorObject;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingItems(): array {
    $mappings = array();
    foreach ($this->mappings as $mapping) {
      $mappings[] = new MappingItem($mapping);
    }
    usort($mappings, function ($a, $b) {
      if ($a->getWeight() == $b->getWeight()) {
          return 0;
      }
      return ($a->getWeight() < $b->getWeight()) ? -1 : 1;
    });

    return $mappings;
  }

  /**
   * {@inheritdoc}
   */
  public function addMappingItem(MappingItem $mapping) {
    $this->mappings[] = $mapping->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function removeMappingsItem($mapping_id) {
    foreach ($this->mappings as $key => $mapping) {
      if ($mapping['id'] == $mapping_id) {
        unset($this->mappings[$key]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateMappingItem(MappingItem $mapping) {
    foreach ($this->mappings as $key => $mapping_item) {
      if ($mapping->getId() == $mapping_item['id']) {
        $this->mappings[$key] = array_replace($this->mappings[$key], $mapping->getConfiguration());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sourceExistsInMappings($source_name): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCandidates(): array {
    $target_candidates = $this->getProcessor()->getTargetCandidates();
    foreach ($this->mappings as $mapping) {
      if (array_key_exists($mapping['target'], $target_candidates)) {
        unset($target_candidates[$mapping['target']]);
      }
    }
    return $target_candidates;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingItem($mapping_id) {
    foreach ($this->mappings as $mapping) {
      if ($mapping['id'] == $mapping_id) {
        return new MappingItem($mapping);
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function import(FileInterface $file, $limit = 0, $offset = 0) {
    $parser = $this->getParser();
    $rows = $parser->parse($file, $this, $limit, $offset);
    $processor = $this->getProcessor();
    $guid_mapping = $this->getGuidMapping();

    $insert = 0;
    $update = 0;
    $total = 0;

    foreach ($rows as $row) {
      $total++;
      $guid = FALSE;
      $entity = FALSE;

      if ($guid_mapping && isset($row[$guid_mapping->getSource()])) {
        $guid = $row[$guid_mapping->getSource()];
        $imported_item = $this->loadImportedItem($guid);
        if ($imported_item && $this->get('update_items')) {
          $processor->updateItem($imported_item->entity_id, $row, $this->mappings);
          $update++;
          continue;
        }
      }
      else {
        $guid = $this->generateUuid();
      }

      if ($this->get('insert_items')) {
        $entity = $processor->insertItem($row, $this->mappings);
        if ($entity) {
          $this->insertRecord($guid, $entity);
          $insert++;
        }
      }
    }

    return [
      'inserted' => $insert,
      'updated' => $update,
      'total' => $total,
    ];
  }

  /**
   *
   */
  private function getConnection() {
    if (!isset($this->database)) {
      $this->database = Database::getConnection();
    }
    return $this->database;
  }

  /**
   *
   */
  public function loadImportedItem($guid) {
    return $this->getConnection()->select('spreadsheet_importer', 'si')
      ->fields('si')
      ->condition('guid', $guid)
      ->execute()
      ->fetch();
  }

  /**
   * Returns guid mapping.
   */
  public function getGuidMapping() {
    foreach ($this->mappings as $mapping) {
      if ($mapping['guid']) {
        return new MappingItem($mapping);
      }
    }
    return FALSE;
  }

  /**
   * Inserts data from an imported item to the import log.
   *
   * @param Entity $entity
   *   Imported entity.
   */
  private function insertRecord($guid, $entity) {
    if (!isset($this->database)) {
      $this->database = Database::getConnection();
    }
    $this->database->insert('spreadsheet_importer')->fields([
      'guid' => $guid,
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'importer' => $this->id(),
      'imported' => time(),
    ])->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition($mapping_id) {
    $mapping = $this->getMappingItem($mapping_id);
    if ($mapping) {
      return $this->getProcessor()->getFieldDefinition($mapping);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasGuidMapping() {
    foreach ($this->mappings as $mapping) {
      if ($mapping['guid']) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingItemBySource($source) {
    $mappings = array();
    foreach ($this->mappings as $mapping) {
      if ($mapping['source'] == $source) {
        return new MappingItem($mapping);
      }
    }
  }

  /**
   * Returns a UUID.
   *
   * @return string
   *   UUID string.
   */
  private function generateUuid() {
    if (!isset($this->uuidService)) {
      $this->uuidService = \Drupal::service('uuid');
    }
    return $this->uuidService->generate();
  }

  /**
   * {@inheritdoc}
   */
  public function export() {
    $parser = $this->getParser();
    $processor = $this->getProcessor();

    $imported_items = $this->getConnection()
      ->select('spreadsheet_importer', 'si')
      ->fields('si')
      ->condition('importer', $this->label())
      ->execute();

    $data = array();
    foreach ($imported_items as $imported_item) {
      $data[] = $processor->exportItem($imported_item->entity_id, $this->getMappingItems());
    }

    $mappings = $this->getMappingItems();
    $row = array();
    foreach ($mappings as $mapping) {
      $row[] = $mapping->getSource();
    }
    array_unshift($data, $row);

    return $parser->export($this, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function template() {
    $mappings = $this->getMappingItems();
    $row = array();
    foreach ($mappings as $mapping) {
      $row[] = $mapping->getSource();
    }
    $parser = $this->getParser();

    return $parser->template($this, $row);
  }

}
