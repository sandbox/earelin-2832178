<?php

namespace Drupal\spreadsheet_importer;

use Drupal\file\FileInterface;

/**
 *
 */
interface ParserInterface {

  /**
   * Parses a spreadsheet file.
   *
   * @param FileInterface $file
   *   File to be parsed.
   * @param SpreadsheetImporterInterface $spreadsheet_importer
   *   Mapping definition.
   * @param int $limit
   *   Max number of rows to return.
   * @param int $offset
   *   Offset to start returning rows.
   *
   * @return array
   *   Keyed array with rows values.
   */
  public function parse(FileInterface $file, SpreadsheetImporterInterface $spreadsheet_importer, $limit = 0, $offset = 0);

  /**
   *
   */
  public function export(SpreadsheetImporterInterface $spreadsheet_importer, array $data);

  /**
   *
   */
  public function template(SpreadsheetImporterInterface $spreadsheet_importer, array $row);

}
