<?php

/**
 * @file
 * Drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function spreadsheet_importer_drush_command() {
  $items = array();

  $items['spreadsheet-importers-import'] = [
    'description' => 'Imports data from a file',
    'arguments' => [
      'importer' => 'The importer name',
      'file' => 'The file for import',
    ],
    'drupal dependencies' => ['custom_drush_command'],
    'aliases' => ['si-import'],
  ];

  $items['spreadsheet-importers-delete'] = [
    'description' => 'Delete imported data',
    'arguments' => [
      'importer' => 'The importer name',
    ],
    'drupal dependencies' => ['custom_drush_command'],
    'aliases' => ['si-delete'],
  ];

  return $items;
}

/**
 * Import data command.
 *
 * @param string $importer
 * @param string $file
 */
function drush_spreadsheet_importer_spreadsheet_importers_import($importer, $file) {

}

/**
 * Delete imported data command.
 *
 * @param string $importer
 */
function drush_spreadsheet_importer_spreadsheet_importers_delete($importer) {

}
